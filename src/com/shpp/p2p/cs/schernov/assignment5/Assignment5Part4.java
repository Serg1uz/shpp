/*
 * File: Assignment5Part4.java
 *_____________________________
 *
 * This file solved task "Parse CSV File"
 * Simple algorithm:
 * 1. Prepare main data:
 *      - csv File use headers or No
 *      - input file name !!!Attention: file must be put in package directory
 * 2. Printing data from file
 *
 * Use CSVReader class
 */
package com.shpp.p2p.cs.schernov.assignment5;

import com.shpp.cs.a.console.TextProgram;

import java.io.File;

public class Assignment5Part4 extends TextProgram {
    // const default file name
    private final static String FILE_NAME = "assignment5part4.csv";
    // csv object from CSVReader class
    CSVReader csv;
    //global variable file used headers or no
    private boolean useHeaders = false;
    //global variable file name
    private String fileName = FILE_NAME;

    /**
     * Main method in program
     */
    public void run() {
        prepareData();
        printResult();
    }

    /**
     * The method prepareData
     * From simple dialog set basic params for program
     */
    private void prepareData() {
        //set headers
        int choice = 0;
        while (choice < 1 || choice > 2) {
            println(choice);
            choice = readInt("In the file will use headers? (1 - yes, 2-no)");
        }
        this.useHeaders = choice == 1;

        //set filename
        fileName = readLine("Input file name (or press ENTER for default file):");
        if (fileName.equals("")) fileName = FILE_NAME;
        while (!checkFileExists()) {
            println("File not found - " + fileName);
            fileName = readLine("Input file name (or press ENTER for default file):");
            if (fileName.equals("")) fileName = FILE_NAME;
        }
    }

    /**
     * The method checkFileExists
     * Checked file is exists in current directory
     * @return boolean True if file exists
     * else False
     */
    private boolean checkFileExists() {
        String baseName = this.getClass().getPackageName();
        fileName = "src/" + baseName.replace('.', '/') + "/" + fileName;
        return new File(fileName).exists();
    }

    /**
     * The method printResult
     * Print data from file use other methods from CSVReader class
     * or error message when reading or parsing file raise error
     */
    private void printResult() {
        csv = new CSVReader(fileName, useHeaders, ';', '"');
        if (csv.getHasError()) {
                        println(csv.getErrorMessage());
        } else {
            println("Get all data :");
            println(csv.getData());
            println("===========================");
            println("Get data from column by Name:");
            println(csv.getDataByKey("Origin"));
            println("===========================");
            println("Get data from column by Column id (Main Task):");
            println(csv.getDataByKey(0));
            println("===========================");
            println("Get data from by range id:");
            println(csv.getDataByRangeId(0, 3));
            println("===========================");
        }
    }
}
