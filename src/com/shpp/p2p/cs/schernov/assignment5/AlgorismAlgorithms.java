/*
 * File: AlgorismAlgorithms.java
 *_____________________________
 *
 * This file solved task "Assembly algorithm"
 * Simple algorithm:
 * 1. Get input data from keyboard two numbers in string format
 * 2. Check validate data
 * 3. calculate result
 *
 * Use CSVReader class
 */

package com.shpp.p2p.cs.schernov.assignment5;

import com.shpp.cs.a.console.TextProgram;


public class AlgorismAlgorithms extends TextProgram {
    //global variables number in string format    
    String n1, n2;
      
    /**
    * The main method in program
    */
    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        /* Sit in a loop, reading numbers and adding them. */
        while (true) {
            n1 = readLine("Enter first number:  ");
            n2 = readLine("Enter second number: ");
            if (checkNumbers(n1) && checkNumbers(n2)) {
                println(n1 + " + " + n2 + " = " + addNumericStrings());
            } else {
                println("Error some number is not digit");
            }
            println();
        }
    }

    /**
     * Given two string representations of nonnegative integers, adds the
     * numbers represented by those strings and returns the result.
     *
     * @return A String representation of n1 + n2
     */
    private String addNumericStrings() {

        int maxLength = Math.max(n1.length(), n2.length());
        n1 = fillNumbers(maxLength, n1);
        n2 = fillNumbers(maxLength, n2);
        int c = 0;
        int d1, d2, r;
        char[] result = new char[maxLength + 1];
        for (int i = maxLength - 1; i >= 0; i--) {
            d1 = Character.getNumericValue(n1.charAt(i));
            d2 = Character.getNumericValue(n2.charAt(i));
            r = d1 + d2 + c;
            if (r < 10) {
                result[i + 1] = Character.forDigit(r, 10);
                c = 0;
            } else {
                result[i + 1] = Character.forDigit((r - 10), 10);
                c = 1;
            }
        }
        if (c == 1) {
            result[0] = '1';
        }
        return String.valueOf(result);
    }
    
    /**
     * The method fillNUmbers
     * Fill 0 chars in left side string to some length
     * @param length int Length to which we increase the string
     * @param str String original string
     * @return String new string with full length
     */
    private String fillNumbers(int length, String str) {
        return "0".repeat(Math.max(0, length - str.length())) +
                str;
    }

    /**
     * The method checkNumber
     * Use simple check every char in string to digit
     * @param str String original string
     * @return boolean true if all char is digit else false
     */
    private boolean checkNumbers(String str) {
        boolean check = true;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (!Character.isDigit(ch)) {
                check = false;
                break;
            }
        }
        return check;
    }
}
