/*
 * File: Assignment5Part3.java
 *_____________________________
 *
 * This file solved task "Game On Road"
 * Simple algorithm:
 * 1. Read dictionary file. Use class CSVReader
 * 2. Input cars number
 * 3. Print word and word count from dictionary when used letter from number.
 * Use regular expression
 */
package com.shpp.p2p.cs.schernov.assignment5;

import com.shpp.cs.a.console.TextProgram;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Assignment5Part3 extends TextProgram {
    //Constant file dictionary
    private final static String DICTIONARY_PATH = "en-dictionary.txt";
    //global variable car number
    String carNumber;
    //global variables words when used letter from car numbers
    private final Set<String> words = new HashSet<>();

    /**
     * Main method in program
     */
    public void run() {
        readFile();
        while (true) {
            carNumber = readLine("Enter cars number:  ");
            carNumber = carNumber.toLowerCase();
            if (checkNumber()) {
                printResults();
            } else {
                println("Number must be in format 1AAA11 with EN letter !!!");
            }

        }
    }

    /**
     * The method checkNumber
     * check car number used mask DLLLDD
     * D - digit
     * L - Letter
     * @return boolean True if car number equal mask
     * else False
     */
    private boolean checkNumber() {
        boolean check = true;
        for (int i = 1; i <= 3; i++) {
            //check letter
            int code = carNumber.charAt(i);
            if (code < 97 || code > 122) {
                check = false;
                break;
            }
        }
        if (check) {
            //check digit
            check = Character.isDigit(carNumber.charAt(0)) && Character.isDigit(carNumber.charAt(4)) && Character.isDigit(carNumber.charAt(5));
        }
        return check;
    }

    /**
     * The method print result search word
     * from dictionary when used letter from number
     */
    private void printResults() {
        List<String> result = searchWords();
        switch (result.size()) {
            case 0 -> println("Can't find words with next letters :" + carNumber.toUpperCase().substring(1, 4));
            case 1 -> {
                println("Find one word:");
                println(result.toString());
            }
            default -> {
                println("Find - " + result.size() + " words:");
                println(result.toString());
            }
        }
    }

    /**
     * The method searchWords
     * Search words from dictionary use
     * regular expression
     * @return List of String
     */
    private List<String> searchWords() {
        carNumber = carNumber.toLowerCase();
        Pattern pattern = Pattern.compile("\\w*(" + carNumber.charAt(1) + ")\\w*(" + carNumber.charAt(2) + ")\\w*(" + carNumber.charAt(3) + ")\\w*");
        List<String> result = words.stream()
                .filter(pattern.asPredicate())
                .sorted()
                .collect(Collectors.toList());
        return result;
    }

    /**
     * The method readFile
     * Read dictionary file to memory
     * use CSVReader library
     */
    private void readFile() {
        String baseName = this.getClass().getPackageName();
        String fileName = "src/" + baseName.replace('.', '/') + "/" + DICTIONARY_PATH;
        if (new File(fileName).exists()) {
            CSVReader csv = new CSVReader(fileName);
            List<String> listWords = csv.getDataByKey(0);
            words.addAll(listWords);
        } else {
            println("can't read file");
        }
    }
}
