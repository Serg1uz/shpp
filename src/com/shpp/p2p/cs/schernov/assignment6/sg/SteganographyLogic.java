package com.shpp.p2p.cs.schernov.assignment6.sg;

import acm.graphics.GImage;

import java.awt.*;

public class SteganographyLogic {
    /**
     * Given a GImage containing a hidden message, finds the hidden message
     * contained within it and returns a boolean array containing that message.
     * <p/>
     * A message has been hidden in the input image as follows.  For each pixel
     * in the image, if that pixel has a red component that is an even number,
     * the message value at that pixel is false.  If the red component is an odd
     * number, the message value at that pixel is true.
     *
     * @param source The image containing the hidden message.
     * @return The hidden message, expressed as a boolean array.
     */
    public static boolean[][] findMessage(GImage source) {
        int[][] pixelArray = source.getPixelArray();
        boolean[][] result = new boolean[pixelArray.length][pixelArray[0].length];
        Color color;
        for (int i = 0; i < pixelArray.length; i++) {
            for (int j = 0; j < pixelArray[0].length; j++) {
                color = new Color(pixelArray[i][j], true);
                result[i][j] = color.getRed() % 2 != 0;
            }
        }
        return result;
    }

    /**
     * Hides the given message inside the specified image.
     * <p/>
     * The image will be given to you as a GImage of some size, and the message will
     * be specified as a boolean array of pixels, where each white pixel is denoted
     * false and each black pixel is denoted true.
     * <p/>
     * The message should be hidden in the image by adjusting the red channel of all
     * the pixels in the original image.  For each pixel in the original image, you
     * should make the red channel an even number if the message color is white at
     * that position, and odd otherwise.
     * <p/>
     * You can assume that the dimensions of the message and the image are the same.
     * <p/>
     *
     * @param message The message to hide.
     * @param source  The source image.
     * @return A GImage whose pixels have the message hidden within it.
     */
    public static GImage hideMessage(boolean[][] message, GImage source) {
        int[][] sourcePixelArray = source.getPixelArray();
        int[][] resultPixelArray = sourcePixelArray.clone();
        Color color;
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < sourcePixelArray.length; i++) {
                for (int j = 0; j < sourcePixelArray[0].length; j++) {
                    color = new Color(sourcePixelArray[i][j], true);
                    int oldRed = color.getRed();
                    int newRed;
                    if (message[i][j]) {
                        if (oldRed % 2 == 0)
                            newRed = oldRed == 0 ? oldRed + 1 : oldRed - 1;
                        else
                            newRed = oldRed;
                    } else {
                        if (oldRed % 2 == 0)
                            newRed = oldRed;
                        else
                            newRed = oldRed - 1;
                    }
                    resultPixelArray[i][j] = new Color(newRed, color.getBlue(), color.getGreen()).getRGB();
                }
            }
        }

        return new GImage(resultPixelArray);
    }
}
