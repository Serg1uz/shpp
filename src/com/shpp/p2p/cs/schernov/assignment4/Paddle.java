/*
 * File: Paddle.java
 *_____________________________
 *
 * A class that physically describes the paddle itself and its behavior
 * Extend of GRect object
 * used option:
 * bricks position and size
 * and methods
 * get top corners , get direction for ball, set  position
 */
package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GPoint;
import acm.graphics.GRect;

import java.awt.*;

public class Paddle extends GRect {
    //Paddle options: position and size
    double x, y, width, height;

    /**
     * Init class
     * and set defaults options
     *
     * @param x      double X-Coordinate
     * @param y      double Y-Coordinate
     * @param width  - double size of paddle (width)
     * @param height - double size of paddle (height)
     */
    public Paddle(double x, double y, double width, double height) {
        super(x, y, width, height);
        this.setColor(Color.BLACK);
        this.setFilled(true);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * The method get top left corner paddle
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getTopLeftCorner() {
        return Helper.getTopLeftCorner(this);
    }

    /**
     * The method get top right corner paddle
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getTopRightCorner() {
        return Helper.getTopRightCorner(this);
    }

    /**
     * The method getPartDirection
     * get direction when the ball touched the paddle
     *
     * @param xCoordinate X-Coordinate (for ball)
     * @return int coefficient for direction and set speed ball
     */
    public double getPartDirection(double xCoordinate) {
        double cornerLeft = this.getTopLeftCorner().getX();
        double partLeft = this.getX() + this.width / 3.0;
        double partRight = partLeft + this.width / 3.0;
        double cornerRight = this.getTopRightCorner().getX();
        if (cornerLeft == xCoordinate) return -2;
        if (cornerRight == xCoordinate) return 2;
        if (partLeft >= xCoordinate) return -1.5;
        if (partRight <= xCoordinate) return 1.5;
        return 1;
    }

    /**
     * The method setPosition
     * set current position for paddle
     *
     * @param xCoordinate X-Coordinate for paddle
     */
    public void setPosition(double xCoordinate) {
        this.setLocation(xCoordinate, y);
    }
}
