/*
 * File: Board.java
 *_____________________________
 *
 * A class that physically describes the board itself and its behavior
 * used option:
 * boards size, board ident, informational labels
 * and methods
 * invert delta XY-Coordinate, start move, move, set default options, set speed move and get points of ball
 */
package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GLabel;
import acm.graphics.GOval;
import acm.graphics.GRect;

import java.awt.*;

public class Board {
    //Boards option
    // board line
    public static final int BOARD_LINE = 5;
    //board size
    double left, right, top, bottom;
    double width, height;
    //board ident from screen
    double leftIdent, topIdent;
    // Information labels
    GLabel scoreLabel, bricksLabel, livesLabel, infoLabel;
    // rectangle for visible board
    GRect leftBorder, topBorder, rightBorder, bottomBorder;

    /**
     * init class
     * and set defaults options
     *
     * @param width     width of board (double)
     * @param height    height of board (double)
     * @param leftIdent left ident (double)
     * @param topIdent  top ident (double)
     **/
    public Board(double width, double height, double leftIdent, double topIdent) {
        left = leftIdent + BOARD_LINE;
        right = left + width - BOARD_LINE;
        top = topIdent + BOARD_LINE;
        bottom = top + height - BOARD_LINE;
        this.width = width;
        this.height = height;
        this.leftIdent = leftIdent;
        this.topIdent = topIdent;
        scoreLabel = scoreLabel();
        bricksLabel = bricksLabel();
        livesLabel = livesLabel();
        infoLabel = infoLabel();
        leftBorder = leftBorder();
        topBorder = topBorder();
        rightBorder = rightBorder();
        bottomBorder = bottomBorder();
    }

    /**
     * The method leftBorder
     * describe left border for board
     *
     * @return GRect object
     */
    public GRect leftBorder() {
        GRect border = new GRect(left - BOARD_LINE, top - BOARD_LINE, BOARD_LINE, height);
        border.setFilled(true);
        border.setColor(Color.DARK_GRAY);
        return border;
    }

    /**
     * The method topBorder
     * describe top border for board
     *
     * @return GRect object
     */
    public GRect topBorder() {
        GRect border = new GRect(left - BOARD_LINE, top - BOARD_LINE, width, BOARD_LINE);
        border.setFilled(true);
        border.setColor(Color.DARK_GRAY);
        return border;
    }

    /**
     * The method rightBorder
     * describe right border for board
     *
     * @return GRect object
     */
    public GRect rightBorder() {
        GRect border = new GRect(right, top - BOARD_LINE, BOARD_LINE, height);
        border.setFilled(true);
        border.setColor(Color.DARK_GRAY);
        return border;
    }

    /**
     * The method bottomBorder
     * describe bottom border for board
     *
     * @return GRect object
     */
    public GRect bottomBorder() {
        GRect border = new GRect(left - BOARD_LINE, bottom, width + BOARD_LINE, BOARD_LINE);
        border.setFilled(true);
        border.setColor(Color.DARK_GRAY);
        return border;
    }

    /**
     * The method borders
     * get all borders for board
     *
     * @return array GRect objects
     */
    public GRect[] borders() {
        return new GRect[]{leftBorder, topBorder, rightBorder, bottomBorder};
    }

    /**
     * The method scorePanel
     * describe score panel
     *
     * @return GRect object
     */
    public GRect scorePanel() {
        GRect panel = new GRect(left - leftIdent, top - BOARD_LINE - topIdent,
                ((leftIdent) * 2 + width) / 3 - BOARD_LINE, topIdent - BOARD_LINE);
        panel.setFilled(false);
        panel.setColor(Color.DARK_GRAY);
        return panel;
    }

    /**
     * The method bricksPanel
     * describe bricks count panel
     *
     * @return GRect object
     */
    public GRect bricksPanel() {
        GRect panel = new GRect(scorePanel().getX() + scorePanel().getWidth(), top - BOARD_LINE - topIdent,
                ((leftIdent) * 2 + width) / 3 - BOARD_LINE, topIdent - BOARD_LINE);
        panel.setFilled(false);
        panel.setColor(Color.DARK_GRAY);
        return panel;
    }

    /**
     * The method livesPanel
     * describe lives count panel
     *
     * @return GRect object
     */
    public GRect livesPanel() {
        GRect panel = new GRect(bricksPanel().getX() + bricksPanel().getWidth(), top - BOARD_LINE - topIdent,
                ((leftIdent) * 2 + width) / 3 - BOARD_LINE, topIdent - BOARD_LINE);
        panel.setFilled(false);
        panel.setColor(Color.DARK_GRAY);
        return panel;
    }

    /**
     * The method infoPanel
     * describe info panel
     *
     * @return GRect object
     */
    public GRect infoPanel() {
        GRect panel = new GRect(left - leftIdent, bottom + BOARD_LINE * 2,
                ((leftIdent) * 2 + width) - BOARD_LINE * 3, topIdent);
        panel.setFilled(false);
        panel.setColor(Color.DARK_GRAY);
        return panel;
    }

    /**
     * The method panels
     * get all panels for board
     *
     * @return array GRect objects
     */
    public GRect[] panels() {
        return new GRect[]{scorePanel(), bricksPanel(), livesPanel(), infoPanel()};
    }

    /**
     * The method scoreLabel
     * describe score info
     *
     * @return GLabel object
     */
    public GLabel scoreLabel() {
        String text = "Score: ";
        scoreLabel = new GLabel(text);
        scoreLabel.setFont("Helvetica-20");
        scoreLabel.setColor(Color.BLUE);
        double x = scorePanel().getX() + BOARD_LINE * 2;
        double y = topIdent / 2 + BOARD_LINE;
        scoreLabel.setLocation(x, y);
        return scoreLabel;
    }

    /**
     * The method bricksLabel
     * describe bricks count info
     *
     * @return GLabel object
     */
    public GLabel bricksLabel() {
        String text = "Bricks: ";
        bricksLabel = new GLabel(text);
        bricksLabel.setFont("Helvetica-20");
        bricksLabel.setColor(Color.RED);
        double x = bricksPanel().getX() + BOARD_LINE * 2;
        double y = topIdent / 2 + BOARD_LINE;
        bricksLabel.setLocation(x, y);
        return bricksLabel;
    }

    /**
     * The method livesLabel
     * describe lives count info
     *
     * @return GLabel object
     */
    public GLabel livesLabel() {
        String text = "Lives: ";
        livesLabel = new GLabel(text);
        livesLabel.setFont("Helvetica-20");
        livesLabel.setColor(new Color(26, 148, 46));
        double x = livesPanel().getX() + BOARD_LINE * 2;
        double y = topIdent / 2 + BOARD_LINE;
        livesLabel.setLocation(x, y);
        return livesLabel;
    }

    /**
     * The method infoLabel
     * describe information label (bottom of screen)
     *
     * @return GLabel object
     */
    private GLabel infoLabel() {
        String text = "Info: ";
        infoLabel = new GLabel(text);
        infoLabel.setFont("Helvetica-20");
        infoLabel.setColor(Color.BLACK);
        double x = infoPanel().getX() + BOARD_LINE * 2;
        double y = topIdent + height + topIdent / 2 + BOARD_LINE * 3;
        infoLabel.setLocation(x, y);
        return infoLabel;
    }

    /**
     * The method labels
     * get all labels from board
     *
     * @return array GLabel objects
     */
    public GLabel[] labels() {
        return new GLabel[]{scoreLabel, bricksLabel, livesLabel, infoLabel};
    }

    /**
     * The method lives
     * visualization of the number of lives in the form of green circles
     * how many are left and how many red ones have been lost
     *
     * @param maxLive     int max live
     * @param currentLive int current live
     * @return array of GOval objects
     */
    public GOval[] lives(int maxLive, int currentLive) {
        GOval[] lives = new GOval[maxLive];
        double x = livesLabel.getX() + livesLabel.getWidth();
        double y = livesLabel.getY() - livesLabel.getHeight() + BOARD_LINE;
        for (int i = 0; i < maxLive; i++) {
            double x1 = x + (20 + BOARD_LINE) * i;
            lives[i] = new GOval(x1, y, 20, 20);
            lives[i].setFilled(true);
            Color color = i < currentLive ? new Color(26, 148, 46) : Color.RED;
            lives[i].setColor(color);
        }
        return lives;
    }
}
