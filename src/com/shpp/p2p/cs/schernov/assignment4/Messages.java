/*
 * File: Messages.java
 *_____________________________
 *
 * A helper class describing messages inside the game
 */
package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.graphics.GRect;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Messages {
    //const  a text for messages
    final static String msgLogo = "BreakOut";
    final static String msgRule = "You must destroy all bricks";
    final static String msgLostLive = "You lost live";
    final static String msgLose = "LOSER";
    final static String msgWin = "WINNER";
    //const a text for buttons
    final static String btnRule = "Rule";
    final static String btnNext = "Next";
    final static String btnStart = "Start";
    final static String btnRestart = "Restart";
    //Message box
    GRect messageBox;
    //Messages text and button
    GLabel msgMain, msgButton;

    /** init method for class
     *
     * @param x double X-Coordinate for messagebox
     * @param y double Y-Coordinate for messagebox
     */
    public Messages(double x, double y) {
        messageBox = messageBox(x, y);
        msgMain = msgMain();
        msgButton = msgButton();
    }

    /**
     * The method objects
     * return main objects for messages (box, text, button)
     * @return array of GObject
     */
    public GObject[] objects() {
        return new GObject[] {messageBox, msgMain, msgButton};
    }

    /**
     *  The method messageBox
     *  return clear rectangle is messagebox background
     * @param x double X-Coordinate for messagebox
     * @param y double Y-Coordinate for messagebox
     * @return GRect object
     */
    public static GRect messageBox(double x, double y) {
        GRect box = new GRect(x-175, y-150, 350, 300);
        box.setColor(Color.BLACK);
        box.setFillColor(Color.WHITE);
        box.setFilled(true);
        return box;
    }

    /**
     * The method msgMain
     * return GLabel for main text message
     * @return GLabel object
     */
    public GLabel msgMain(){
        GLabel label = new GLabel("BreakOut");
        label.setFont("Helvetica-50");
        double x = messageBox.getX() + (350-label.getWidth())/2;
        double y = messageBox.getY() + label.getHeight() * 2;
        label.setLocation(x, y);
        return label;
    }
    /**
     * The method msgMainLocation
     * centered main message text by X-Coordinate
     */
    private void msgMainLocation(){
        double x = messageBox.getX() + (350-msgMain.getWidth())/2;
        double y = messageBox.getY() + 50;
        msgMain.setLocation(x, y);
    }
    /**
     * The method msgButton
     * return GLabel for button text
     * and used override mouse event for mouseEntered
     * @return GLabel object
     */
    public GLabel msgButton(){
        GLabel label = new GLabel("START");
        label.setColor(Color.RED);
        label.setFont("Helvetica-25");
        double x = messageBox.getX() + (350-label.getWidth())/2;
        double y = messageBox.getY() + messageBox.getHeight() - label.getHeight();
        label.setLocation(x, y);
        label.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                //
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                //
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                label.setColor(Color.green);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                label.setColor(Color.RED);
            }
        });
        return label;
    }

    /**
     * The method showMessage
     * set data for main message text and button
     * @param type String type of message (start, rule, lost, lose, win)
     */
    public void showMessage(String type){
        switch (type) {
            case "start":
                msgMain.setLabel(msgLogo);
                msgMain.setColor(Color.RED);
                msgMain.setFont("Helvetica-50");
                msgMainLocation();

                msgButton.setLabel(btnRule);
                break;
            case "rule":
                msgMain.setLabel(msgRule);
                msgMain.setColor(Color.blue);
                msgMain.setFont("Helvetica-25");
                msgMainLocation();

                msgButton.setLabel(btnNext);
                break;
            case "lost":
                msgMain.setLabel(msgLostLive);
                msgMain.setColor(Color.red);
                msgMain.setFont("Helvetica-35");
                msgMainLocation();

                msgButton.setLabel(btnStart);
                break;
            case "lose":
                msgMain.setLabel(msgLose);
                msgMain.setColor(Color.red);
                msgMain.setFont("Helvetica-50");
                msgMainLocation();

                msgButton.setLabel(btnRestart);
                break;
            case "win":
                msgMain.setLabel(msgWin);
                msgMain.setColor(Color.green);
                msgMain.setFont("Helvetica-50");
                msgMainLocation();

                msgButton.setLabel(btnRestart);
                break;
            default:
                break;
        }
    }
}
