/*
 * File: Helper.java
 *_____________________________
 *
 * class describing helper methods
 * get random value or boolean, get corners of figure
 */

package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GObject;
import acm.graphics.GPoint;
import acm.util.RandomGenerator;

public class Helper {
    /** method return random double number of range from min value to max value **/
    public static double getRandom(double minValue, double maxValue){
        return RandomGenerator.getInstance().nextDouble(minValue, maxValue);
    }

    /** method return random boolean used coefficient **/
    public  static  boolean getRandom (double coefficient){
        return RandomGenerator.getInstance().nextBoolean(coefficient);
    }
    /** method return GPoint XY-Coordinates top left corner of object **/
    public static GPoint getTopLeftCorner(GObject object){
        return new GPoint(object.getX(), object.getY());
    }
    /** method return GPoint XY-Coordinates top right corner of object **/
    public static GPoint getTopRightCorner(GObject object){
        return new GPoint(object.getX()+object.getWidth(), object.getY());
    }

}
