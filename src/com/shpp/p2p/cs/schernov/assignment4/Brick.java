/*
 * File: Brick.java
 *_____________________________
 *
 * A class that physically describes the brick itself and its behavior
 * Extend of GRect object
 * used option:
 * bricks position, size and color, count of hit point and point, identifier brick
 * and methods
 * invert delta XY-Coordinate, start move, move, set default options, set speed move and get points of ball
 */

package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GRect;

import java.awt.*;

public class Brick extends GRect {
    //Brick option
    //brick XY - coordinate
    double x,y;
    //brick size
    double width, height;
    //brick color
    Color color;
    //brick position in board
    int row, column;
    //brick point
    int point;
    //brick hit point
    int hp;
    //brick identifier
    boolean isBrick = true;

    /** Init class brick
     * and set default options
     * @param x double X-Coordinate
     * @param y double Y-Coordinate
     * @param width double size(width) of brick
     * @param height double size(height) of brick
     * @param color Color color of brick
     * @param row double X position in board
     * @param column double Y position in board
     */
    public Brick (double x, double y, double width, double height, Color color, int row, int column){
        super (x, y, width, height);
        this.setColor(Color.BLACK);
        this.setFillColor(color);
        this.setFilled(true);

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.row = row;
        this.column = column;
        setBricks();
    }

    /** The method setBricks
     * set hit point and point for brick by color
     */
    private void setBricks() {
        if (this.color == Color.cyan) {
            this.hp = 1;
            this.point = 1;
        }
        if (this.color == Color.green) {
            this.hp = 2;
            this.point = 1;
        }
        if (this.color == Color.yellow) {
            this.hp = 1;
            this.point = 2;
        }
        if (this.color == Color.orange) {
            this.hp = 2;
            this.point = 2;
        }
        if (this.color == Color.red) {
            this.hp = 2;
            this.point = 5;
        }
    }

    /** The method kickBrick
     * describing the behavior of hitting a brick by
     * returning the number of points
     * @return  int point of brick
     */
    public int kickBrick() {
        switch (this.hp){
            case 1:
                this.hp --;
                break;
            case 2:
                this.hp--;
                this.setFilled(false);
                break;
            default:
                break;
        }
        return point;
    }
}
