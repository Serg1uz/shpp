/*
 * File: Assignment1Part1.java
 *
 * Solves task "Collect newspaper" :
 * Karel must leave the house and pick up a newspaper on the doorstep
 *
 * A simple algorithm for solving the  task:
 * 1. Collect Newspaper
 * 2. Return to start position
 *
 * Used ExtendedKarel class
 */

package com.shpp.p2p.cs.schernov.assignment1;

public class Assignment1Part1 extends ExtendedKarel {
    /**
     * Specifies the program entry point
     */
    public void run() throws Exception {
        collectNewspaper();
        backToStart();
        sayFinish();
    }

    /**
     * Method collect of newspaper         *
     */
    private void collectNewspaper() throws Exception {
       turnRight();
       moveExt();
       turnLeft();
       while (noBeepersPresent())
            moveExt();
       pickBeeper();
    }

    /**
     * Method returned Karel to start position
     */
    private void backToStart() throws Exception {
       turnAround();
       while (!frontIsBlocked())
           moveExt();
       turnRight();
       moveExt();
       turnRight();
    }
}
