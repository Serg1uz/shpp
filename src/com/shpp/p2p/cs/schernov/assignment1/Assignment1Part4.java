/*
 * File: Assigment1Part4
 *
 * Solves task "CheckBoard" :
 * Karel must put beepers in the board by check bord scheme
 *
 * A simple algorithm for solving the  task:
 * Karel move on the board by columns scheme and put beepers
 * into odd cell
 */
package com.shpp.p2p.cs.schernov.assignment1;


public class Assignment1Part4 extends ExtendedKarel {
    /**
     * Main method for running program
     */
    public void run() throws Exception {
        createChessBoard();
        sayFinish();
    }

    /**
     * Method for fill chess board
     */
    private void createChessBoard() throws Exception {
        fillFirstLine();
        fillColumns();
    }

    /**
     * Method for fill first line
     */
    private void fillFirstLine() throws Exception {
        putBeeper();
        fillLine();
        turnLeft();
    }

    /**
     * Method for fill all columns
     */
    private void fillColumns() throws Exception {
        while (frontIsClear()) {
            fillLine();
            moveToFirstLine();
            turnRight();
            moveToNextColumn();
        }
    }

    /**
     * Fill line in current direction
     */
    private void fillLine() throws Exception {
        while (frontIsClear()) {
            if(noBeepersPresent()) {
                moveExt();
                putBeeper();
            } else {
                moveExt();
            }
        }
    }

    /**
     * Move left to next column if front is clear
     */
    private void moveToNextColumn() throws Exception {
        if (frontIsClear()) {
            move();
            turnRight();
        }
    }

    /**
     * Move Karel to first Line
     */
    private void moveToFirstLine() throws Exception {
        turnAround();
        while (frontIsClear())
            moveExt();
    }
}
