/*
 * File: Assigment1Part2
 *
 * Solves task "Stone Mason" :
 * Karel must fill all columns
 *
 * A simple algorithm for solving the  task:
 * 1. Fill column
 * 2. Skip 4 columns
 * 3. Repeat until not completed all board
 *
 * Used ExtendedKarel class
 */

package com.shpp.p2p.cs.schernov.assignment1;


public class Assignment1Part2 extends ExtendedKarel {
    /**
     * Main method for running program
     */
    public void run() throws Exception {
        buildColumns();
        sayFinish();
    }

    /**
     * Build columns by beepers
     */
    private void buildColumns() throws Exception {
        buildColumn(); //first column
        do {
            moveToNextPart();
            buildColumn();
        } while (frontIsClear());
    }

    /**
     * Move to next stage for build column
     */
    private void moveToNextPart() throws Exception {
        for (int i = 0; i < 4; i += 1) {
            moveExt();
        }
    }

    /**
     * Put Beeper if it's doesn't exist into cell
     */
    private void savedPutsBeepers() throws Exception {
        if (noBeepersPresent())
             putBeeper();
    }

    /**
     * Build column by beepers
     */
    private void buildColumn() throws Exception {
        turnLeft();
        savedPutsBeepers(); // first beeper into column
        do{
            moveExt();
            savedPutsBeepers();
        } while (frontIsClear());
        moveToFirstLine();
    }

    /**
     * Move Karel to first Line
     */
    private void moveToFirstLine() throws Exception {
        turnAround();
        while (frontIsClear())
            moveExt();
        turnLeft();
    }
}
