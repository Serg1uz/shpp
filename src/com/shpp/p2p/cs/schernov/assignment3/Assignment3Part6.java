/*
 *  File: Assignment3part6.java
 *_____________________________
 * Solves task "Animation" :
 * 5 seconds animation
 * complete logo SHPP from many puzzles
 *
 * A simple algorithm for solving the  task:
 * 1. draw background
 * 2. generated puzzles
 * 3. show puzzles in  start position
 * 4. show animation when all puzzles not in finish position
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignment3;

import acm.graphics.GArc;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.util.Random;

class PacMan extends GArc {
    private static final double START_POSITION_X = 0;
    private static final double START_POSITION_Y = 0;
    private static final double WIDTH = 100;
    private static final double HEIGHT = 100;

    int theta_sign = 1;
    int theta_delta = 5;
    double theta_angle = 0;




    public PacMan(){
        super(START_POSITION_X, START_POSITION_Y, WIDTH, HEIGHT, 0, 360);
        super.setFilled(true);
        super.setColor(Color.black);
        super.setFillColor(Color.yellow);
    }

    public void moved(double dx, double dy) {
        this.move(dx, dy);
        calcStartAngle();
        this.setStartAngle(theta_angle);
        this.setSweepAngle(calcSweepAngel());
//        System.out.println("Current X - " + this.getX()+ " Start - " + this.getStartAngle() + " Sweep - " + this.getSweepAngle());
    }

    private double calcSweepAngel() {
        return 360 - 2 * theta_angle;
    }

    private void calcStartAngle() {
        theta_angle += theta_sign * theta_delta;
        if (theta_angle == 0 || theta_angle == 45) theta_sign = -theta_sign;
    }

    public void setNewRow() {
        this.setLocation(0-WIDTH, this.getY()+HEIGHT);
    }
}

public class Assignment3Part6 extends WindowProgram {
    //Global const for size screen DONT CHANGE
    public static final int APPLICATION_WIDTH = 850;
    public static final int APPLICATION_HEIGHT = 420;
    //Global const for count of steps animation
    public static final int STEPS = 70;

    //Global variables for start position
    double firstX, firstY;

    //Global variables for count of Puzzle
    int index = 0;

    PacMan pacman = new PacMan();

    /**
     * Specifies the program entry point
     */
    public void run() {
//        getCentered();
//        drawBackGround();
        add(pacman);
//        generateShBoxes();
//        generatePlus(firstX + 10 * 38, firstY + 10 * 10);
//        generatePlus(firstX + 10 * 58, firstY + 10 * 10);
//        drawPicture();
//        showMotion();
        for (int i = 0; i < 5000; i++) {
            pacman.moved(5, 0);
            pause(1000/24);
            if (pacman.getX() > getWidth()) pacman.setNewRow();
        }
//        pacman.setStartAngle(30);
//        pacman.setSweepAngle(300);
    }

    /**
     * The method drawBackGround
     * draw rectangle to all screen
     * with black filled color and logo
     */
    private void drawBackGround() {
        drawRectangle(0, 0, getWidth(), getHeight(), Color.black);
        drawLogo();
    }

    /**
     * The method draw school's logo
     */
    private void drawLogo(){
        Color color = Color.blue;
        drawRectangle(firstX, firstY, 60, 210, color);
        drawRectangle(firstX+60 +80, firstY, 60, 210, color);
        drawRectangle(firstX+2*60+80*2, firstY, 60, 210, color);
        drawRectangle(firstX, 260, 60*3+80*2, 50, color);

        color = Color.green;
        double x = firstX+3*60+80*2+20;
        double y = 180;
        drawRectangle( x, y, 150, 50, color);
        double x1 = x + 150  +20;
        drawRectangle(x1, y, 150, 50, color);
        x1 = x + 50;
        double y1 = y - 50;
        drawRectangle(x1, y1, 50, 150, color);
        x1 = x + 150  +20 +50;
        drawRectangle(x1, y1, 50, 150, color);
    }
    /**
     * The method getCentered
     * calculate center of picture
     * and set global variables for start position
     */
    private void getCentered() {
        this.firstX = (getWidth() - 700) / 2.0;
        this.firstY = (getHeight() - 200) / 2.0;
    }

    /**
     * The method randomCoordinate
     *
     * @param maxValue double max value
     * @return double random number from range 10 .. Max Value
     */
    private double randomCoordinate(double maxValue) {
        Random r = new Random();
        return 10 + (maxValue - 10) * r.nextDouble();
    }





    /**
     * The method drawRectangle
     * draw  rectangle
     *
     * @param x      double start X-Coordinate rectangle
     * @param y      double start Y-Coordinate rectangle
     * @param width  double size width
     * @param height double size height
     * @param color  Color color of rectangle
     */
    private void drawRectangle(double x, double y, double width, double height, Color color) {
        GRect rect = new GRect(x, y, width, height);
        rect.setFilled(true);
        rect.setColor(color);
        add(rect);
    }
}
